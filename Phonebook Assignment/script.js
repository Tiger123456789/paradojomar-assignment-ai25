var users = [
    {
        id: 1,
        name: "Jomar",
        phonenumber: "09542843684",
    },
    {
        id: 2,
        name: "Berna",
        phonenumber: "09375243628",
    },
	{
        id: 3,
        name: "Marie",
        phonenumber: "093275685645",
    }
];

$.each(users, function (i, user) {
    appendToUsrTable(user);
});

$("form").submit(function (e) {
    e.preventDefault();
});

$("form#addUser").submit(function () {
    var user = {};
    var nameInput = $('input[name="name"]').val().trim();
    var phonenumberInput = $('input[name="phonenumber"]').val().trim();
    if (nameInput && phonenumberInput) {
        $(this).serializeArray().map(function (data) {
            user[data.name] = data.value;
        });
        var lastUser = users[Object.keys(users).sort().pop()];
        user.id = lastUser.id + 1;

        addUser(user);
    } else {
        alert("All fields must have a valid value.");
    }
});

function addUser(user) {
    users.push(user);
    appendToUsrTable(user);
}

function deleteUser(id) {
    var action = confirm("Are you sure you want to delete this Contact?");
    var msg = "Contact deleted successfully!";
    users.forEach(function (user, i) {
        if (user.id == id && action != false) {
            users.splice(i, 1);
            $("#userTable #user-" + user.id).remove();
            flashMessage(msg);
        }
    });
}


function flashMessage(msg) {
    $(".flashMsg").remove();
    $(".row").prepend(`
          <div class="col-sm-12"><div class="flashMsg alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button> <strong>${msg}</strong></div></div>
      `);
}

function appendToUsrTable(user) {
    $("#userTable > tbody:last-child").append(`
          <tr id="user-${user.id}">
              <td class="userData" name="name">${user.name}</td>
              '<td class="userData" name="phonenumber">${user.phonenumber}</td>
              '<td align="center">
                  <button class="btn btn-dark form-control" style = "background-color: red;" onClick="deleteUser(${user.id})">DELETE</button>
              </td>
          </tr>
      `);
}
